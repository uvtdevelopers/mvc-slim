<?php
/**
 * Created by PhpStorm.
 * User: Cristian
 * Date: 4/17/2019
 * Time: 7:44 PM
 */

namespace App\Controllers;

use App\App;

class HomeController extends App
{
    public function index($request, $response, $args){
        return $this->view->render($response, "home/index.twig");
    }
}