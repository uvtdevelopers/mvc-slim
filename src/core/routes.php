<?php
/**
 * Created by PhpStorm.
 * User: Cristian
 * Date: 4/17/2019
 * Time: 7:30 PM
 */

$app->get("/", "HomeController:index")->setName("home");
$app->group("/account", function () use($app){
    $app->get("/login", "AccountController:login")->setName("login");
    $app->post("/actionLogin", "AccountController:actionLogin")->setName("actionLogin");
    $app->get("/register", "AccountController:register")->setName("register");
    $app->post("/actionRegister", "AccountController:actionRegister")->setName("actionRegister");
});